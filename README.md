# Simple Table

Todo
* Sort By Column
* Search By Column
* pagination by data not from response


## Options
```
{
    data: 'data.data',
    columns: [
        {
            'name' : 'id',
            'data' : 'id'
        },
        {
            'name' : 'name',
            'data' : 'name'
        },
        {
            'name' : 'action',
            'data' : null,
            'render' : function(value, data) {
                return '<button class="btn btn-info" onclick="onEdit(this)" value="'+data.id+'">Edit</button><button class="btn btn-danger" onclick="onDelete(this)" value="'+data.id+'">Delete</button>'
            }
        }
    ],
    search: {
        query: 'key'
    },
    pagination: {
        pageKey: 'page',
        data: 'data',
        next: {
            data: 'next_page_url',
            key: 'page'
        },
        prev: {
            data: 'prev_page_url',
            key: 'page'
        },
        urlToPageNum: true
    }
}

```
#
## Data

data = data dari response

example 
```
{
    "data" : {
        "data" : [
            ...post
        ]
    },
    "meta" : {
        ...
    }
}
```

contoh memasukan nya akan seperti ini
```
data: 'data.data'
```
#
## Columns
columns adalah list column yang akan di ambil

example
```
{
    "data" : {
        "data" : 
        [
            "id" : 1,
            "name" : "Dinar"
        ], 
        [
            "id" : 2,
            "name" : "Hamid"
        ]
    },
    "meta" : {
        ...
    }
}
```

contoh setting nya akan seperti ini

```
columns: [
    {
        name: 'id',
        data: 'id'
    },
    {
        name: 'name',
        data: 'name'
    }
]
```

render berfungsi untuk mengubah data yang di ambil dan disesuaikan sesuai keperluan.

example

```
columns: [
    {
        name: 'id',
        data: 'id'
    },
    {
        name: 'name',
        data: 'name',
        render: function(value, data) {
            return value.toUpperCase()
        }
    }
]
```
value adalah value dari key name sedangkan data adalah value dari array yang berisi semua data
contoh lain
```
{
    name: 'name',
    data: 'name',
    render: function(value, data) {
        return data.id + " " + data.name
    }
}
```
#
## search
```
{
    search: {
        query: 'cari'
    }
}
```
example: https://domain.com/api/category?cari=apa

#
## Pagination

```
    pagination: {
        pageKey: 'page',
        data: 'data',
        next: {
            data: 'next_page_url',
        },
        prev: {
            data: 'prev_page_url',
        },
        urlToPageNum: false
    }
```

pageKey = page yang di pakai di backend
example: https://domain.com/api/category?page=2

data = data dari response

example 
```
{
    "data" : {
        "data" : [
            ...post
        ],
        "next_page_url" : 2,
        "prev_page_url" : 1
    },
    "meta" : {
        ...
    }
}
```

next.data = dari response seperti di atas
prev.data = dari response seperti di atas
next.key dan prev.key di gunakan ketika urlToPageNum di pakai
example response
```
{
    "data" : {
        "data" : [
            ...post
        ],
        "next_page_url" : "https://domen/api/category?page=2",
        "prev_page_url" : "https://domen/api/category?page=1"
    },
    "meta" : {
        ...
    }
}
```
dari data di atas kita harus membuat option pagination seperti ini

```
    pagination: {
        pageKey: 'page',
        data: 'data',
        next: {
            data: 'next_page_url',
            key: 'page'
        },
        prev: {
            data: 'prev_page_url',
            key: 'page'
        },
        urlToPageNum: true
    }
```

jika pagination response nya menggunakan angka/integer maka option yang harus di pakai seperti yang pertama dan setting urlToPageNum ke false