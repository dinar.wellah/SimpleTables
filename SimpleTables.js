/**
 * PT Lazycore Inovasi Digital
 * @author Dinar Hamid
 * @version 1.0
 * @note Datatables better than this.
 */

const SimpleTables = class {

    columns = null

    url = null

    method = 'GET'

    options = {}

    async start(url, method = 'GET', options = {}) {

        if(typeof options['columns'] !== 'undefined' && typeof options['columns'] == 'object') {
            this.columns = options['columns']
        } 

        if(!options['pagination']['urlToPageNum']) {
            options['pagination']['urlToPageNum'] = false;
        }

        var data = await this.request(url, method, options)
        if(typeof data.response === 'string' && data.status === 200) {
            var resp = JSON.parse(data.response)
            if(typeof options['pagination']['data'] !== 'undefined') {
                options['pagingResponse'] = this.getNestedObject(options['pagination']['data'], resp)
            }
            
            document.getElementsByTagName('tbody')[0].innerHTML = ''
            this.options = options
            await this.pagination_render()
            await this.executeTable(this.getNestedObject(options['data'], resp))
        }
        
        for(var keys of ['search', 'next', 'prev']) {
            recreateNode(document.getElementById(keys))
        }
        
        this.url = url
        this.method = method
        this.options = options
        this.search()
        this.pagination()

    }

    search() {
        var options = this.options
        var url = this.url
        var method = this.method

        document.getElementById('search').addEventListener('keyup', () => {
            options['query'] = {}
            options['query'][options['search']['query']] = document.getElementById('search').value

            new SimpleTables().start(url, method, options)
        }, {
            once: true
        })

    }

    getPageNum(value, key) {
        
        if(value === null || typeof value === 'undefined') {
            return null
        }

        var url = new URL(value)
        if (url.searchParams.has(key)) {
            return url.searchParams.get(key)
        }
        return null
    }

    pagination() {
        var options = this.options
        var url = this.url
        var method = this.method

        if (typeof options['query'] === 'undefined') {
            options['query'] = {}
        }

        const eventNext = () => {

            var getSearch = document.getElementById('search').value
            
            if(getSearch !== '') {
                options['query'][options['search']['query']] = document.getElementById('search').value
            }
            
            if(options['pagination']['query']['next']) {
                options['query'][options['pagination']['pageKey']] = options['pagination']['query']['next']
            }

            new SimpleTables().start(url, method, options)
        }

        const eventPrev = () => {
            
            var getSearch = document.getElementById('search').value

            if(getSearch !== '') {
                options['query'][options['search']['query']] = document.getElementById('search').value
            }

            if(options['pagination']['query']['prev']) {
                options['query'][options['pagination']['pageKey']] = options['pagination']['query']['prev']
            }
            
            new SimpleTables().start(url, method, options)
        }
        
        document.getElementById('next').addEventListener('click', eventNext, {
            once: true
        })
        document.getElementById('prev').addEventListener('click', eventPrev,{
            once: true
        })
    }

    request(url, method='GET', options={}) {
        return new Promise(function(resolve, reject) {

            if(typeof options['query'] === 'object' && Object.keys(options['query']).length > 0) {
                var q = []
                for(var keys in options['query']) {
                    q.push(keys + "=" + options['query'][keys])
                }
                url = url + '?' + q.join('&')
            }

            var xhr = new XMLHttpRequest()
            xhr.open(method, url)
    
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

            if(typeof options['headers'] === 'object' && Object.keys(options['headers']).length > 0) {
                for(var h in options['headers']) {
                    xhr.setRequestHeader(h, options['headers'][h])
                }
            }
    
            if(typeof options['body'] === 'object' && Object.keys(options['body']).length > 0) {
                var q = []
                for(var keys in options['body']) {
                    q.push(keys + "=" + options['body'][keys])
                }
                body = q.join('&')
            }
            xhr.onload = function() {
                if(this.readyState == 4) {
                    resolve({
                        status: xhr.status,
                        response: xhr.responseText
                    })
                } else {
                    reject({
                        status: xhr.status,
                        statusText: xhr.statusText
                    })
                }
            }
            xhr.onerror = function() {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                })
            }

            if(typeof options['body'] === 'object' && Object.keys(options['body']).length > 0) {
                xhr.send(body)
            } else {
                xhr.send()
            }
        })
    }

    async pagination_render() {
        this.options['pagination']['query'] = {}
        for(var key of ['next', 'prev']) {
            var kk = document.getElementById(key)
            kk.setAttribute('href', '#')
            var num = this.getNestedObject(this.options['pagination'][key]['data'], this.options['pagingResponse'])
            if(typeof num !== 'undefined' || num !== null) {
                if(this.options['pagination']['urlToPageNum']) {
                    num = this.getPageNum(num, this.options['pagination'][key]['key'])
                }
                if(num === null) {
                    kk.setAttribute("disabled","disabled")
                }
                if(num !== null) {
                    kk.removeAttribute("disabled")
                }
                this.options['pagination']['query'][key] = num
            }
        }
    }

    async executeTable(data) {

        if(typeof data === 'undefined' || data === null) {
            return
        }

        var element = document.getElementsByTagName('table')
        var thead
        var tbody
        for(var i=0;i<element[0].children.length;i++) {
            if (element[0].children.item(i).tagName == 'THEAD') {
                thead = element[0].children.item(i)
                continue
            }
            if (element[0].children.item(i).tagName == 'TBODY') {
                tbody = element[0].children.item(i)
                continue
            }
        }
        
        if(!thead.isConnected) {
            throw new Error("Please declare thead and make sure add tr and th")
        }

        if(!tbody.isConnected) {
            tbody = document.createElement('tbody')
            element.appendChild(tbody)
        }


        var listCreatedTr = {}
        for(var i=0;i<data.length;i++) {
            listCreatedTr[i] = document.createElement('tr')

            for(var j=0;j<this.columns.length;j++) {
                
                if(this.columns[j].data !== null) {
                    if (this.columns[j].name.includes('.') || this.columns[j].data.includes('.')) {
                        var td = document.createElement('td')
                        var value = this.getNestedObject(this.columns[j].data, data[i])
                        if(typeof this.columns[j].render === 'function') {
                            td.innerHTML = this.columns[j].render(value, data[i])
                        } else {
                            td.innerText = value
                        }
                        listCreatedTr[i].appendChild(td)
                        continue
                    }
                    for(var key in data[i]) {
                        var cleanK = key.toLowerCase().replace('-','').replace(' ','').replace('_','')
                        if (this.columns[j].name === cleanK) {
                            var td = document.createElement('td')
                            if(typeof this.columns[j].render === 'function') {
                                td.innerHTML = this.columns[j].render(data[i][key], data[i])
                            } else {
                                td.innerText = data[i][key]
                            }
                            listCreatedTr[i].appendChild(td)
                        }
                    } 
                } else if (this.columns[j].data === null && typeof this.columns[j].render === 'function') {
                    var td = document.createElement('td')
                    td.innerHTML = this.columns[j].render(null, data[i])
                    listCreatedTr[i].appendChild(td)
                }
            }

        }

        Object.keys(listCreatedTr).map((z) => tbody.appendChild(listCreatedTr[z]))

    }
 
    getNestedObject(path, obj) {
        return path.split('.').reduce(function(prev, curr) {
            return prev ? prev[curr] : null
        }, obj || self)
    }
    
}

function recreateNode(el, withChildren) {
    if (withChildren) {
      el.parentNode.replaceChild(el.cloneNode(true), el);
    }
    else {
      var newEl = el.cloneNode(false);
      while (el.hasChildNodes()) newEl.appendChild(el.firstChild);
      el.parentNode.replaceChild(newEl, el);
    }
}