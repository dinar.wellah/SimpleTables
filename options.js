
// options pass to SimpleTables
const options = {
    data: 'data.data',
    columns: [
        {
            'name' : 'id',
            'data' : 'id'
        },
        {
            'name' : 'name',
            'data' : 'name'
        },
        {
            'name' : 'action',
            'data' : null,
            'render' : function(value, data) {
                return '<button class="btn btn-info" onclick="onEdit(this)" value="'+data.id+'">Edit</button><button class="btn btn-danger" onclick="onDelete(this)" value="'+data.id+'">Delete</button>'
            }
        }
    ],
    search: {
        query: 'key'
    },
    pagination: {
        pageKey: 'page',
        data: 'data',
        next: {
            data: 'next_page_url',
            key: 'page'
        },
        prev: {
            data: 'prev_page_url',
            key: 'page'
        },
        urlToPageNum: true
    }
}